module.exports = {
    extends: [
        'plugin:react/recommended',
        'standard'
    ],
    globals: {
        describe: true,
        test: true,
        expect: true
    }
};