import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducer from './reducers'

/**
 * @author Jordy Koppen
 *
 * Create redux application store and apply redux-thunk middleware
 */

const store = createStore(
  reducer,
  applyMiddleware(thunk)
)

export default store
