import React from 'react'
import PropTypes from 'prop-types'
import Poster from '../Poster'
import withShows from '../../containers/withShows'

/**
 * @author Jordy Koppen
 *
 * A component wrapped with the withShow HOC which handles the redux state
 * and actions for the show list and selected show.
 *
 * @param {boolean} isFetching - For showing a loading spinner.
 * @param {array} data - An array containing the shows from api call.
 * @param {function} setSelectedShow - A redux action that changes the currently selected show.
 */

const Shows = ({ isFetching, data, setSelectedShow }) => (
  <React.Fragment>
    { data && !isFetching &&
      <div className="shows">
        { data.map((show) => <Poster key={show.id} {...show} onClick={() => setSelectedShow(show)} />) }
      </div>
    }
    { isFetching &&
      <div className="loading">
        <div className="loader" />
      </div>
    }
  </React.Fragment>
)

Shows.propTypes = {
  isFetching: PropTypes.bool,
  data: PropTypes.array,
  setSelectedShow: PropTypes.func
}

export default withShows(Shows)
