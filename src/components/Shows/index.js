/**
 * @author Jordy Koppen
 *
 * Import handled in here so we have named component files
 */
import Shows from './Shows'
import './Shows.scss'
export default Shows
