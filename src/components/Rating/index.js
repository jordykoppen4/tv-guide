/**
 * @author Jordy Koppen
 *
 * Import handled in here so we have named component files
 */
import Rating from './Rating'
import './Rating.scss'
export default Rating
