import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'

/**
 * @author Jordy Koppen
 * 
 * Component for turning a rating number
 * 1 to 10 into a star rating on a 
 * scale of 1 to 5
 */

class Rating extends PureComponent {
  /**
   * Declare proptypes
   */
  static propTypes = {
    stars: PropTypes.number
  }

  static defaultPropTypes = {
    stars: 0
  }

  /**
   * Method to convert the rating into the amount of stars
   * and build an array of elements.
   */
  renderStars () {
    const amountOfStars = Math.round(this.props.stars) / 2
    const toRender = []

    for (let i = 0; i < 5; i++) {
      if (amountOfStars - i > 0.5) {
        toRender.push(<i key={i} className="fas fa-star"/>)
      } else if (amountOfStars - i === 0.5) {
        toRender.push(<i key={i} className="fas fa-star-half-alt"/>)
      } else {
        toRender.push(<i key={i} className="far fa-star"/>)
      }
    }

    return toRender
  }

  render () {
    return (
      <div className="rating">
        { this.renderStars() }
      </div>
    )
  }
}

export default Rating
