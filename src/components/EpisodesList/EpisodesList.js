import React from 'react'
import PropTypes from 'prop-types'
import withEpisodes from '../../containers/withEpisodes'

/**
 * @author Jordy Koppen
 *
 * Functional component to render the Episodes.
 * Wrapped with a HOC which contains the redux state and actions
 */

const EpisodesList = ({ isFetching, data: episodes }) => (
  <div className="episodes-list">
    {/**
      * Check if episodes exists and map trough them
      * to build the list
      */}
    { episodes &&
      episodes.map(ep =>
        <div key={ep.id} className="episode">
          { ep.image &&
            <img src={ep.image.medium} alt={ep.name}/>
          }
          <div className="episode-details">
            <h3>{ep.name}</h3>
            <p dangerouslySetInnerHTML={{ __html: ep.summary }} />
          </div>
        </div>
      )
    }
    {/**
      * Whilst the API call is happening, we
      * display a loader
      */}
    { isFetching &&
      <div className="loading">
        <div className="loader" />
      </div>
    }
  </div>
)

EpisodesList.propTypes = {
  data: PropTypes.array,
  isFetching: PropTypes.bool
}

export default withEpisodes(EpisodesList)
