/**
 * @author Jordy Koppen
 *
 * Import handled in here so we have named component files
 */
import EpisodesList from './EpisodesList'
import './EpisodesList.scss'
export default EpisodesList
