/**
 * @author Jordy Koppen
 *
 * Import handled in here so we have named component files
 */
import Poster from './Poster'
import './Poster.scss'
export default Poster
