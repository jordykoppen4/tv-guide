import React from 'react'
import PropTypes from 'prop-types'

/**
 * @author Jordy Koppen
 *
 * Poster functional component for displaying
 * tv show covers.
 *
 * @param {object} image - contains cover image url's for medium and original size.
 * @param {event} onClick - Binds onClick event.
 */

const Poster = ({ image, onClick }) => (
  <div className="poster" onClick={onClick}>
    <figure className="poster__image" style={{ backgroundImage: `url('${image.medium}')` }} />
  </div>
)

Poster.propTypes = {
  image: PropTypes.object,
  onClick: PropTypes.func
}

export default Poster
