/**
 * @author Jordy Koppen
 *
 * Import handled in here so we have named component files
 */
import NavBar from './NavBar'
import './NavBar.scss'
export default NavBar
