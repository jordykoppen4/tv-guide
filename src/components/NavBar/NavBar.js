import React from 'react'
import { Link } from 'react-router-dom'

/**
 * @author Jordy Koppen
 *
 *  Navbar functional component which implements a react-router-link
 */

const NavBar = () => (
  <nav className="navbar">
    <Link to="/" className="navbar-link">Tv Shows</Link>
  </nav>
)

export default NavBar
