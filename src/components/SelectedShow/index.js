/**
 * @author Jordy Koppen
 *
 * Import handled in here so we have named component files
 */
import SelectedShow from './SelectedShow'
import './SelectedShow.scss'
export default SelectedShow
