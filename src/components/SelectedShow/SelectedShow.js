import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import withSelectedShow from '../../containers/withSelectedShow'
import Rating from '../Rating'

/**
 * @author Jordy Koppen
 *
 * Funtional component for the selected show section.
 *
 * @param {object} data - Contains the data for the currently selected show.
 */
const SelectedShow = ({ data }) => (
  <div className="selected-show">
    { data &&
      <div className="selected-show__details">
        <img src={data.image.original} alt={data.name} />
        <div className="selected-show__details__content">
          <h1>{ data.name }</h1>
          <Rating stars={data.rating.average} />
          <div
            dangerouslySetInnerHTML={{ __html: data.summary }}
            className="selected-show__details__content__summary"
          />
          <Link to={`/episodes/${data.id}`} className="episodes-button" >Go to episode list</Link>
        </div>
      </div>
    }
  </div>
)

SelectedShow.propTypes = {
  data: PropTypes.object
}

export default withSelectedShow(SelectedShow)
