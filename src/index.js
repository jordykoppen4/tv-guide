import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import store from './store'
import registerServiceWorker from './registerServiceWorker'
import './index.scss'

/**
 * Bootstrap the React application to the
 * DOM
 */
ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('root'))
registerServiceWorker()
