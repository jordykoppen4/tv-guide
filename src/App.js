import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import './App.scss'
import Routes from './Routes'
import NavBar from './components/NavBar'

/**
 * @author Jordy Koppen
 *
 * Function Component which straps the react-router and
 * NavBar component.
 */
const App = () => (
  <BrowserRouter>
    <React.Fragment>
      <NavBar />
      <Routes />
    </React.Fragment>
  </BrowserRouter>
)

export default App
