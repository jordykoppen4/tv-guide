import Types from './types'

const initialState = {
  isFetching: false,
  data: null,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.SHOWS_FETCHING:
      return {...state, isFetching: true}
    case Types.SHOWS_FETCHING_SUCCESS:
      return {...state, isFetching: false, data: action.payload}
    case Types.SHOWS_FETCHING_ERROR:
      return {...state, isFetching: false, error: action.payload}
    default:
      return state
  }
}
