import HttpClient from '../../utils/HttpClient'
import Types from './types'

/**
 * @author Jordy Koppen
 *
 * Fetch list of Tv Shows.
 */
const getShows = () => async (dispatch) => {
  dispatch({ type: Types.SHOWS_FETCHING })

  try {
    const response = await HttpClient.get()
    dispatch({ type: Types.SHOWS_FETCHING_SUCCESS, payload: response.data })
  } catch (e) {
    dispatch({
      type: Types.SHOWS_FETCHING_ERROR,
      payload: {
        message: e.response.message,
        status: e.resposne.status
      }
    })
  }
}

export {
  getShows
}
