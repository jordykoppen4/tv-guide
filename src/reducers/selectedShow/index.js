import Types from './types'

const initialState = {
  isFetching: false,
  data: null,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.SELECTED_SHOW_FETCHING:
      return {...state, isFetching: true}
    case Types.SELECTED_SHOW_FETCHING_SUCCESS:
      return {...state, isFetching: false, data: action.payload}
    case Types.SELECTED_SHOW_FETCHING_ERROR:
      return {...state, isFetching: false, error: action.payload}
    case Types.SET_SELECTED_SHOW:
      return {...state, data: action.payload}
    default:
      return state
  }
}
