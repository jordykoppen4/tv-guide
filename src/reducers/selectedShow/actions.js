import HttpClient from '../../utils/HttpClient'
import Types from './types'

/**
 * @author Jordy Koppen
 *
 * Pass the clicked show to the redux state to display on seleted show.
 * @param {object} show - show data
 */
const setSelectedShow = (show) => async (dispatch) => {
  dispatch({ type: Types.SET_SELECTED_SHOW, payload: show })
}

/**
 * Action for fetching the show on initial load
 *
 * @param {number} id - show id.
 */
const fetchSelectedShow = (id) => async (dispatch) => {
  dispatch({ type: Types.SELECTED_SHOW_FETCHING })

  try {
    const url = `${id}`
    const response = await HttpClient.get(url)

    dispatch({ type: Types.SELECTED_SHOW_FETCHING_SUCCESS, payload: response.data })
  } catch (e) {
    dispatch({
      type: Types.SELECTED_SHOW_FETCHING_ERROR,
      payload: {
        message: e.response.message,
        status: e.resposne.status
      }
    })
  }
}

export {
  setSelectedShow,
  fetchSelectedShow
}
