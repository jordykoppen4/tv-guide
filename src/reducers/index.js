import { combineReducers } from 'redux'
import shows from './shows'
import selectedShow from './selectedShow'
import episodes from './episodes'

const reducer = combineReducers({
  shows,
  selectedShow,
  episodes
})

export default reducer
