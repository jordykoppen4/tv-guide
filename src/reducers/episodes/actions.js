import HttpClient from '../../utils/HttpClient'
import Types from './types'

/**
 * @author Jordy Koppen
 *
 * Action for fetching the episodes for given show
 *
 * @param {number} showId - Id for show
 */
const fetchEpisodes = (showId) => async (dispatch) => {
  dispatch({ type: Types.EPISODES_FETCHING })

  try {
    const url = `/${showId}/episodes`
    const response = await HttpClient.get(url)

    dispatch({ type: Types.EPISODES_FETCHING_SUCCESS, payload: response.data })
  } catch (e) {
    dispatch({
      type: Types.SELECTED_SHOW_FETCHING_ERROR,
      payload: {
        message: e.response.message,
        status: e.resposne.status
      }
    })
  }
}

/**
 * Clear the episodes state.
 */
const clearEpisodes = () => async (dispatch) => {
  dispatch({ type: Types.EPISODES_CLEAR })
}

export {
  fetchEpisodes,
  clearEpisodes
}
