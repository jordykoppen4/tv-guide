import Types from './types'

const initialState = {
  isFetching: false,
  data: null,
  error: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.EPISODES_FETCHING:
      return {...state, isFetching: true}
    case Types.EPISODES_FETCHING_SUCCESS:
      return {...state, isFetching: false, data: action.payload}
    case Types.EPISODES_FETCHING_ERROR:
      return {...state, isFetching: false, error: action.payload}
    case Types.EPISODES_CLEAR:
      return {...state, isFetching: false, data: null}
    default:
      return state
  }
}
