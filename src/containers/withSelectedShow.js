import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchSelectedShow } from '../reducers/selectedShow/actions'

/**
 * @author Jordy Koppen
 *
 * A HOC for managing the Selected show state.
 */
export default (ComposedComponent) => {
  class SelectedShow extends Component {
    /**
     * When first load, fetch data for
     * Westworld (1371)
     */
    componentDidMount () {
      this.props.fetchSelectedShow(1371)
    }

    /**
     * Render the wrapped component with the data for the selected show.
     */
    render () {
      return <ComposedComponent {...this.props.selectedShow} {...this.props} />
    }
  }

  /**
   *
   * @param {object} selectedShow - Data for currently selected show.
   */
  const mapStateToProps = ({ selectedShow }) => ({ selectedShow })

  return connect(mapStateToProps, { fetchSelectedShow })(SelectedShow)
}
