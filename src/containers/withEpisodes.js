import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchEpisodes, clearEpisodes } from '../reducers/episodes/actions'

/**
 * @author Jordy Koppen
 *
 * A HOC for managing the Episodes list state.
 */
export default (ComposedComponent) => {
  class Episodes extends Component {
    /**
     * When component mounted, fire the action to
     * fetch the episodes.
     */
    componentDidMount () {
      this.props.fetchEpisodes(this.props.showId)
    }

    /**
     * When the component will unmount (on route change for example)
     * clear the currently selected show episodes so it wont show up
     * for a brief second when a different show is selected afterwards.
     */
    componentWillUnmount () {
      this.props.clearEpisodes()
    }

    /**
     * Render the wrapped component with the props from
     * redux state
     */
    render () {
      return <ComposedComponent {...this.props.episodes} />
    }
  }

  /**
   *
   * @param {array} episodes - An array containing all the episodes for selected show.
   * @returns {episodes}
   */
  const mapStateToProps = ({ episodes }) => ({ episodes })

  return connect(mapStateToProps, { fetchEpisodes, clearEpisodes })(Episodes)
}
