import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getShows } from '../reducers/shows/actions'
import { setSelectedShow } from '../reducers/selectedShow/actions'

/**
 * @author Jordy Koppen
 *
 * A HOC for managing the Shows state.
 */
export default (ComposedComponent) => {
  class Shows extends Component {
    /**
     * When component mounts, fetch all the shows.
     */
    componentDidMount () {
      this.props.getShows()
    }

    /**
     * Render the wrapped component with the shows.
     */
    render () {
      return <ComposedComponent {...this.props.shows} {...this.props} />
    }
  }

  /**
   *
   * @param {array} shows - An array with all the shows.
   */
  const mapStateToProps = ({ shows }) => ({ shows })

  return connect(mapStateToProps, {
    getShows,
    setSelectedShow
  })(Shows)
}
