import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Home from './views/Home'
import Episodes from './views/Episodes'

/**
 * @author Jordy Koppen
 *
 * A component which renders all routes
 * specified in the routeMap object.
 */

const routeMap = [
  {
    component: Home,
    exact: true,
    path: '/',
    title: 'Home'
  }, {
    component: Episodes,
    path: '/episodes/:id',
    title: 'Episodes'
  }
]

const Routes = () => (
  <Switch>
    { routeMap.map((route, index) => <Route key={index} {...route} />) }
  </Switch>
)

export default Routes
