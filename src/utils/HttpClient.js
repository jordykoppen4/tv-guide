import axios from 'axios'

/**
 * @author Jordy Koppen
 *
 * Create Axios base instance.
 */

const HttpClient = axios.create({
  baseURL: 'https://api.tvmaze.com/shows',
  timeout: 1000
})

export default HttpClient
