import React from 'react'
import Shows from '../components/Shows'
import SelectedShow from '../components/SelectedShow'

const Home = () => (
  <React.Fragment>
    <SelectedShow />
    <Shows />
  </React.Fragment>
)

export default Home
