import React from 'react'
import PropTypes from 'prop-types'
import EpisodesList from '../components/EpisodesList'

const Episodes = ({ match: { params } }) => <EpisodesList showId={params.id} />

Episodes.propTypes = {
  match: PropTypes.object
}

export default Episodes
